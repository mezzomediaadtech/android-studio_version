package com.mplus.sdk.sample.release;

import com.mapps.android.share.AdInfoKey;
import com.mapps.android.view.EndingAdView;
import com.mz.common.listener.AdListener;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class EndingDialog extends Dialog {
	private TextView mTitleView;
	private Button mLeftButton;
	private Button mRightButton;
	private String mTitle;
	private EndingAdView m_flexibleAD = null;

	private View.OnClickListener mLeftClickListener;
	private View.OnClickListener mRightClickListener;

	private String p, m, s;

	private int mediaType;

	private Context mContext;

	public void setCode(int p, int m, int s, int mediaType) {
		this.p = String.valueOf(p);
		this.m = String.valueOf(m);
		this.s = String.valueOf(s);
		this.mediaType = mediaType;
	}

	public EndingDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		mContext = context;
	}

	public EndingDialog(Context context, String title, View.OnClickListener singleListener) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		this.mTitle = title;
		this.mLeftClickListener = singleListener;
		mContext = context;
	}

	public EndingDialog(Context context, String title, View.OnClickListener leftListener, View.OnClickListener rightListener) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		this.mTitle = title;
		this.mLeftClickListener = leftListener;
		this.mRightClickListener = rightListener;
		mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.8f;
		getWindow().setAttributes(lpWindow);

		setContentView(R.layout.ending_dialog);

		setLayout();
		setTitle(mTitle);
		setClickListener(mLeftClickListener, mRightClickListener);
	}

	private void setTitle(String title) {
		mTitleView.setText(title);
	}

	private void setClickListener(View.OnClickListener left, View.OnClickListener right) {
		if (left != null && right != null) {
			mLeftButton.setOnClickListener(left);
			mRightButton.setOnClickListener(right);
		} else if (left != null && right == null) {
			mLeftButton.setOnClickListener(left);
		} else {

		}
	}

	private void setLayout() {
		FrameLayout ending_ad = (FrameLayout) findViewById(R.id.ending_ad);
		mTitleView = (TextView) findViewById(R.id.tv_title);
		mLeftButton = (Button) findViewById(R.id.bt_left);
		mRightButton = (Button) findViewById(R.id.bt_right);
		m_flexibleAD = new EndingAdView(mContext, mediaType);
		m_flexibleAD.setUserAgeLevel("0");
		m_flexibleAD.setStoreurl("http://store.url");
		m_flexibleAD.setKeyword("");
		m_flexibleAD.setExternal("");
		m_flexibleAD.setAdViewCode(p, m, s);
		m_flexibleAD.setBannerSize(250, 300);
		m_flexibleAD.setAdListener(new AdListener() {

			public void onFailedToReceive(View arg0, int errorCode) {
				Utils.customErrorMsg(errorCode, new Handler() {
					@Override
					public void dispatchMessage(Message msg) {
						String log = String.valueOf(msg.obj);
						Utils.log(log);
					}
				});
				if(errorCode != AdInfoKey.AD_SUCCESS){
					dismiss();
				}
			}

			public void onChargeableBannerType(View arg0, boolean bcharge) {
				Utils.onChargeableBannerType(bcharge);
			}

			public void onInterClose(View v) {
				// TODO Auto-generated method stub

			}

			public void onAdClick(View v) {
				dismiss();
				Utils.log("on method Click");
			}
		});
		m_flexibleAD.setAccount("id");
		m_flexibleAD.setEmail("few.com");
		ending_ad.addView(m_flexibleAD);
		if (m_flexibleAD != null) {
			m_flexibleAD.startEndingAdView(); // windowID가 flexible과 다른 경우에 셋팅
		}
	}

	@Override
	public void dismiss() {
		if (m_flexibleAD != null) {
			m_flexibleAD.onDestroy();
		}
		super.dismiss();
	}

	public EndingAdView getM_flexibleAD() {
		return m_flexibleAD;
	}

	public void setM_flexibleAD(EndingAdView m_flexibleAD) {
		this.m_flexibleAD = m_flexibleAD;
	}
}
