package com.mezzomedia.mplus;

import com.mapps.android.share.AdInfoKey;
import com.mapps.android.view.AdView;
import com.mz.common.listener.AdListener;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class BannerSample extends Activity implements AdListener {

	private AdView m_adView = null;
	private Handler handler = new Handler();
	private LinearLayout bLayout = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_banner);

		String p = getIntent().getStringExtra(Constants.P);
		String m = getIntent().getStringExtra(Constants.M);
		String s = getIntent().getStringExtra(Constants.S);
		boolean isAni = getIntent().getBooleanExtra(Constants.ANI, true);
		int type = getIntent().getIntExtra(Constants.TYPE, AdView.TYPE_IMAGE);
		// createBannerJavaCodeView(p, m, s, type, isAni);
		createBannerXMLMode();
	}
	
	private void createBannerXMLMode() {
		m_adView = (AdView) findViewById(R.id.ad);
		m_adView.setUserAge("1");
		m_adView.setUserGender("3");
		m_adView.setEmail("few.com");
		m_adView.setAccount("id");
		m_adView.setUserAgeLevel("0");
		m_adView.setStoreurl("http://store.url");
		m_adView.setKeyword("");
		m_adView.setExternal("");
		m_adView.setAdListener(this);
		m_adView.isAnimateImageBanner(true);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (m_adView != null)
			m_adView.StartService();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (m_adView != null)
			m_adView.StopService();
	}

	@Override
	protected void onDestroy() {
		m_adView = null;
		if (bLayout != null) {
			bLayout.removeAllViews();
		}

		super.onDestroy();
	}

	public void onInterClose(View v) {
		// TODO Auto-generated method stub

	}

	public void onAdClick(View v) {
		// TODO Auto-generated method stub

	}

	public void onChargeableBannerType(View v, boolean bcharge) {
		// TODO Auto-generated method stub
		// 수신된 광고가 무료 광고인 경우 bcharge 값이 false 임.
		if (m_adView == v) {
			if (bcharge) {
				Log.v("ADSDK", "-------> chargeable advertise !!!");
			} else {
				Log.v("ADSDK", "-------> Non chargeable advertise !!");
			}
		}
	}

	public void onFailedToReceive(View v, int errorCode) {
		// TODO Auto-generated method stub
		// 광고 수신 성공한 경우 호출됨.수신값 0은 성공을 의미함
		if (m_adView == v) {
			Log.e("ADSDK", "-------> errorCode=" + errorCode);
			final int errcode = errorCode;
			handler.post(new Runnable() {
				public void run() {
					// TODO Auto-generated method stub
					showErrorMsg(errcode);
				}

			});
		}
	}

	private void showErrorMsg(int errorCode) {
		switch (errorCode) {
		case 0:
			Log.e("ADSDK", "-------> Code=" + errorCode);
			break;
		case AdInfoKey.NETWORK_ERROR:
			debug("NETWORK_ERROR");
			break;
		case AdInfoKey.AD_SERVER_ERROR:
			debug("SERVER_ERROR");
			break;
		case AdInfoKey.AD_API_TYPE_ERROR:
			debug("API_TYPE_ERROR");
			break;
		case AdInfoKey.AD_APP_ID_ERROR:
			debug("APP_ID_ERROR");
			break;
		case AdInfoKey.AD_WINDOW_ID_ERROR:
			debug("WINDOW_ID_ERROR");
			break;
		case AdInfoKey.AD_ID_BAD:
			debug("AD_ID_BAD");
			break;
		case AdInfoKey.AD_ID_NO_AD:
			debug("AD_ID_NO_AD");
			break;
		case AdInfoKey.AD_CREATIVE_ERROR:
			debug("CREATIVE_ERROR");
			break;
		case AdInfoKey.AD_ETC_ERROR:
			debug("AD_ETC_ERROR");
			break;

		case AdInfoKey.AD_ADCLICK:
			debug("AD_ADCLICK");
			break;

		case AdInfoKey.AD_INTERVAL:
			debug("AD_INTERVAL");
			break;

		case AdInfoKey.AD_BACKGROUND_CALL
			// wrong call
			break;

		default:
			break;
		}
	}
	
	private void createBannerJavaCodeView(String p, String m, String s, int type, boolean isAni) {
		if (m_adView == null) {
			try {
				m_adView = new AdView(BannerSample.this, 0, 0, type);
				m_adView.setAdViewCode(p, m, s);
				m_adView.setUserAge("1");
				m_adView.setUserGender("3");
				m_adView.setEmail("few.com");
				m_adView.setAccount("id");
				m_adView.setUserAgeLevel("0");
				m_adView.setStoreurl("http://store.url");
				m_adView.setKeyword("");
				m_adView.setExternal("");
				m_adView.setAdListener(this); // 리스너 등록
				m_adView.isAnimateImageBanner(isAni); // 에니메이션 Flag
				bLayout = (LinearLayout) findViewById(R.id.layout_gallery);
				bLayout.addView(m_adView);
			} catch (Exception e) {
				m_adView = null;
				e.printStackTrace();
			}
		}
	}
	
	private void debug(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}


}
