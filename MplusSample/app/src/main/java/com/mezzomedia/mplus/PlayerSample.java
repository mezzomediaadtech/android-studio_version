package com.mezzomedia.mplus;

import com.mapps.android.share.AdInfoKey;
import com.mapps.android.view.AdVideoPlayer;
import com.mz.common.listener.AdVideoPlayerErrorListener;
import com.mz.common.listener.AdVideoPlayerListener;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

public class PlayerSample extends Activity implements AdVideoPlayerListener, AdVideoPlayerErrorListener {
	AdVideoPlayer adPlayer = null;

	private Handler handler = new Handler();
	private LinearLayout mbottomLayout = null;
	private VideoView m_videoView = null;
	private boolean bActivate = true;
	private LinearLayout rootVideo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_player);
		mbottomLayout = (LinearLayout) findViewById(R.id.layout_bottom);
		m_videoView = (VideoView) findViewById(R.id.videoViewExample);
		rootVideo = (LinearLayout) findViewById(R.id.layout_gallery);
		String p = getIntent().getStringExtra(Constants.P);
		String m = getIntent().getStringExtra(Constants.M);
		String s = getIntent().getStringExtra(Constants.S);
		// aplusPlay();
		createJavaPlay(p, m, s);
		String mediaURL = "http://vod.midas-i.com/20131010171708_middle.mp4";
		m_videoView.setVideoURI(Uri.parse(mediaURL));
		m_videoView.start();
	}

	// private void aplusPlay(){
	// adPlayer = (AdVideoPlayer) findViewById(R.id.ad_player);
	// if(adPlayer!=null){
	// adPlayer.setAdViewCode(p, m, s);
	// adPlayer.setCateContent("", "");
	// adPlayer.setUserAge("1");
	// adPlayer.setUserGender("2");
	// adPlayer.setLoaction(true);
	// adPlayer.setAccount("id");
	// adPlayer.setEmail("few.com");
	// adPlayer.setAdVideoPlayerErrorListner(this);
	// adPlayer.setAdVideoPlayerListner(this);
	// adPlayer.setVideoMode(AdVideoPlayer.MODE_WIDE);
	// adPlayer.showAd();
	// }
	// }

	private void createJavaPlay(String p, String m, String s) {
		rootVideo.removeAllViews();
		adPlayer = new AdVideoPlayer(PlayerSample.this);
		adPlayer.setAdViewCode(p, m, s);
		adPlayer.setCateContent("", "");
		adPlayer.setUserAge("1");
		adPlayer.setUserGender("2");
		adPlayer.setLoaction(true);
		adPlayer.setAccount("id");
		adPlayer.setEmail("few.com");
		adPlayer.setUserAgeLevel("0");
		adPlayer.setStoreurl("http://store.url");
		adPlayer.setKeyword("");
		adPlayer.setExternal("");
		adPlayer.setAdVideoPlayerErrorListner(this);
		adPlayer.setAdVideoPlayerListner(this);
		adPlayer.setVideoMode(AdVideoPlayer.MODE_WIDE);
		rootVideo.addView(adPlayer);
		adPlayer.showAd();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (adPlayer != null)
			adPlayer.onDestory();

		rootVideo.removeAllViews();
		adPlayer = null;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		bActivate = false;
		if (adPlayer != null)
			adPlayer.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!bActivate) {
			if (adPlayer != null)
				adPlayer.onResume();
		}
	}

	@TargetApi(Build.VERSION_CODES.ECLAIR)
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (adPlayer != null)
			adPlayer.onBackPressed();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		System.out.println("onConfigurationChanged");
		switch (newConfig.orientation) {
		case Configuration.ORIENTATION_PORTRAIT:
			if (mbottomLayout != null)
				mbottomLayout.setVisibility(View.VISIBLE);
			break;
		case Configuration.ORIENTATION_LANDSCAPE:
			if (mbottomLayout != null)
				mbottomLayout.setVisibility(View.GONE);
			break;

		}
	}

	public void onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		final int nwhat = what;
		handler.post(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
				setMsg("VideoError=" + Integer.toString(nwhat));
			}

		});
	}

	public void onAdPlayerReceive(View view, int recvCode) {
		// TODO Auto-generated method stub
		if (adPlayer != null) {
			if (adPlayer == view) {
				final int errcode = recvCode;
				handler.post(new Runnable() {
					public void run() {
						// TODO Auto-generated method stub
						setMsg(errcode);
					}

				});
			}
		}
	}

	private void setMsg(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		if (m_videoView != null) {
			m_videoView.setVisibility(View.VISIBLE);
			m_videoView.start();
		}
	}

	private void setMsg(int errorCode) {
        String log;
        switch (errorCode) {
            ///////////////////////////////////////////////////////////////////////////////////
            case AdInfoKey.AD_PLAYER_COMPLETE:
                log = "[ " + errorCode + " ] " + "영상 광고 완료";
                eventAction();
                break;
            case AdInfoKey.AD_PLAYER_AD_START:
                log = "[ " + errorCode + " ] " + "영상광고 시작";
                break;
            case AdInfoKey.AD_PLAYER_SKIP:
                log = "[ " + errorCode + " ] " + "영상광고 스킵";
                eventAction();
                break;
            case AdInfoKey.AD_ADCLICK:
                log = "[ " + errorCode + " ] " + "영상광고 클릭";
                eventAction();
                break;
            case AdInfoKey.AD_PLAYER_INCOMING_CALL:
                log = "[ " + errorCode + " ] " + "전화 중...";
                eventAction();
                break;
            case AdInfoKey.AD_PLAYER_FINISH_OTHER_REASON:
                log = "[ " + errorCode + " ] " + "예외상황 영상 종료";
                eventAction();
                break;
            case AdInfoKey.AD_PLAYER_AD_LOGO_CLICK:
                log = "[ " + errorCode + " ] " + "영상 로고 클릭";
                eventAction();
                break;
            ///////////////////////////////////////////////////////////////////////////////////
            case AdInfoKey.NETWORK_ERROR:
                log = "[ " + errorCode + " ] " + "(ERROR)네트워크";
                eventAction();
                break;
            case AdInfoKey.AD_SERVER_ERROR:
                log = "[ " + errorCode + " ] " + "(ERROR)서버";
                eventAction();
                break;
            case AdInfoKey.AD_API_TYPE_ERROR:
                log = "[ " + errorCode + " ] " + "(ERROR)API 형식 오류";
                eventAction();
                break;
            case AdInfoKey.AD_APP_ID_ERROR:
                log = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
                eventAction();
                break;
            case AdInfoKey.AD_WINDOW_ID_ERROR:
                log = "[ " + errorCode + " ] " +"(ERROR)ID 오류";
                eventAction();
                break;
            case AdInfoKey.AD_ID_BAD:
                log = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
                eventAction();
                break;
            case AdInfoKey.AD_CREATIVE_ERROR:
                log = "[ " + errorCode + " ] " + "(ERROR)광고 생성 불가";
                eventAction();
                break;
            case AdInfoKey.AD_ID_NO_AD:
                log = "[ " + errorCode + " ] " + "광고 소진";
                eventAction();
                break;
            case AdInfoKey.AD_INTERVAL:
                log = "[ " + errorCode + " ] " + "어뷰징";
                eventAction();
                break;
            case AdInfoKey.AD_TIMEOUT:
                log = "[ " + errorCode + " ] " + "광고 API TIME OUT";
                eventAction();
                break;
            case AdInfoKey.CREATIVE_FILE_ERROR:
                log = "[ " + errorCode + " ] " + "파일 형식 오류";
                eventAction();
                break; 
			case AdInfoKey.AD_BACKGROUND_CALL
				// wrong call
				break;
				
            default:
                log = "[ " + errorCode + " ] " + "etc";
                eventAction();
                break;
        }
        debug(log);
	}

	public void onAdPlayerDurationReceive(View arg0, int sec) {
		// TODO Auto-generated method stub
		// 총 광고소재물 시간 콜백
//		System.out.println("=onAdPlayerDurationReceive= : " + (sec / 1000) + "sec");

		new Thread(new Runnable() {
			public void run() {
				if(adPlayer!=null){
					while (adPlayer!=null && adPlayer.getVideoCurrentDuration() != -1) {
						// 총 광고소재물 재생 시간 가져오기
//						System.out.println((adPlayer.getVideoCurrentDuration() / 1000) + "sec");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}	
				}
			}
		}).start();
	}
	
	private void eventAction(){
		if (m_videoView != null) {
			m_videoView.setVisibility(View.VISIBLE);
			m_videoView.start();
		}
	}
	
	private void debug(String msg) {
		Toast.makeText(PlayerSample.this, msg, Toast.LENGTH_LONG).show();
	}
}
