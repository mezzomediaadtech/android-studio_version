package com.mezzomedia.mplus;

import com.mapps.android.share.AdInfoKey;
import com.mapps.android.view.AdInterstitialView;
import com.mapps.android.view.AdView;
import com.mz.common.listener.AdListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements AdListener {

	private Handler handler = new Handler();
	private AdInterstitialView m_interView = null;
	private EndingDialog mEndingDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		((TextView) findViewById(R.id.version)).setText("Sample");

		btnSetting();

	}

	private void btnSetting() {
		Button btn_banner1 = (Button) findViewById(R.id.btn_banner);
		btn_banner1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				bannerView(100, 200, 300, true, AdView.TYPE_HTML);
			}
		});
		btn_banner1.setVisibility(View.VISIBLE);
		Button btn_interstital1 = (Button) findViewById(R.id.btn_interstital);
		btn_interstital1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				interView(100, 200, 301);
			}
		});
		btn_interstital1.setVisibility(View.VISIBLE);

		Button btn_ending1 = (Button) findViewById(R.id.btn_ending);
		btn_ending1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				endingView(100, 200, 302, AdView.TYPE_HTML);
			}
		});
		btn_ending1.setVisibility(View.VISIBLE);

		Button btn_movie1 = (Button) findViewById(R.id.btn_movie);
		btn_movie1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				playerView(100, 200, 303);
			}
		});
		btn_movie1.setVisibility(View.VISIBLE);
	}

	private void interView(int p, int m, int s) {
		if (m_interView == null) {
			m_interView = new AdInterstitialView(MainActivity.this);
			m_interView.setAdListener(MainActivity.this);
			m_interView.setViewStyle(AdInterstitialView.VIEWSTYLE.RESIZE);
			m_interView.setUserAge("22");
			m_interView.setUserGender("1");
			m_interView.setLoaction(true);
			m_interView.setAccount("test");
			m_interView.setEmail("test@mezzomediaco.kr");
			m_interView.setUserAgeLevel("0");
			m_interView.setStoreurl("http://store.url");
			m_interView.setKeyword("");
			m_interView.setExternal("");
		}
		m_interView.setAdViewCode(String.valueOf(p), String.valueOf(m), String.valueOf(s));
		m_interView.ShowInterstitialView();
	}

	private void bannerView(final int p, final int m, final int s, final boolean isAni, final int type) {
		new Thread(new Runnable() {
			public void run() {
				Intent intent = new Intent(MainActivity.this, BannerSample.class);
				intent.putExtra(Constants.P, String.valueOf(p));
				intent.putExtra(Constants.M, String.valueOf(m));
				intent.putExtra(Constants.S, String.valueOf(s));
				intent.putExtra(Constants.ANI, isAni);
				intent.putExtra(Constants.TYPE, type);
				startActivity(intent);
			}
		}).start();

	}

	private void endingView(final int p, final int m, final int s, int media) {
		mEndingDialog = new EndingDialog(MainActivity.this, "테스트 종료베너", leftClickListener2, rightClickListener2);
		mEndingDialog.setCode(p, m, s, media);
		mEndingDialog.show();
	}

	private void playerView(final int p, final int m, final int s) {
		new Thread(new Runnable() {

			public void run() {
				Intent intent = new Intent(MainActivity.this, PlayerSample.class);
				intent.putExtra(Constants.P, String.valueOf(p));
				intent.putExtra(Constants.M, String.valueOf(m));
				intent.putExtra(Constants.S, String.valueOf(s));
				startActivity(intent);
			}
		}).start();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mEndingDialog != null) {
			mEndingDialog.dismiss();
		}
		if (m_interView != null) {
			m_interView.onDestroy();
			m_interView = null;
		}

	}

	private OnClickListener leftClickListener2 = new OnClickListener() {
		public void onClick(View v) {
			if (mEndingDialog != null) {
				mEndingDialog.dismiss();
				mEndingDialog = null;
			}
			finish();
		}

	};
	private OnClickListener rightClickListener2 = new OnClickListener() {
		public void onClick(View v) {
			if (mEndingDialog != null) {
				mEndingDialog.dismiss();
				mEndingDialog = null;
			}

		}

	};

	// 광고 유료/무료
	public void onChargeableBannerType(View v, final boolean is) {
		handler.post(new Runnable() {
			public void run() {
				debug((is) ? "no chargebale" : "chargeble");
			}

		});
	}

	public void onFailedToReceive(View v, int errorCode) {
		final int errcode = errorCode;
		handler.post(new Runnable() {
			public void run() {
				showErrorMsg(errcode);
			}

		});
	}

	private void showErrorMsg(int errorCode) {
		String log;
		switch (errorCode) {
		case AdInfoKey.AD_SUCCESS:
			log = "[ " + errorCode + " ] " + "광고 성공";
			break;
		case AdInfoKey.AD_ID_NO_AD:
			log = "[ " + errorCode + " ] " + "광고 소진";
			break;
		case AdInfoKey.NETWORK_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)네트워크";
			break;
		case AdInfoKey.AD_SERVER_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)서버";
			break;
		case AdInfoKey.AD_API_TYPE_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)API 형식 오류";
			break;
		case AdInfoKey.AD_APP_ID_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
			break;
		case AdInfoKey.AD_WINDOW_ID_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
			break;
		case AdInfoKey.AD_ID_BAD:
			log = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
			break;
		case AdInfoKey.AD_CREATIVE_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)광고 생성 불가";
			break;
		case AdInfoKey.AD_ETC_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)예외 오류";
			break;
		case AdInfoKey.CREATIVE_FILE_ERROR:
			log = "[ " + errorCode + " ] " + "(ERROR)파일 형식";
			break;
		case AdInfoKey.AD_INTERVAL:
			log = "[ " + errorCode + " ] " + "광고 요청 어뷰징";
			break;
		case AdInfoKey.AD_TIMEOUT:
			log = "[ " + errorCode + " ] " + "광고 API TIME OUT";
			break;
		case AdInfoKey.AD_ADCLICK:
			log = "[ " + errorCode + " ] " + "광고 클릭";
			break;
		case AdInfoKey.AD_BACKGROUND_CALL
			// wrong call
			break;

		default:
			log = "[ " + errorCode + " ] " + "etc";
			break;
		}
		debug(log);
	}

	private void debug(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	// 전면 광고 닫기 이벤트
	public void onInterClose(View v) {
		debug("전면 광고 닫기");
	}

	public void onAdClick(View v) {
		// TODO Auto-generated method stub

	}

}
